# isgd

[![License](https://is.gd/GbWFMI)](https://opensource.org/licenses/ISC)

### Motivation
Wanted a simple CLI tool for created shortened URLs via the [is.gd](https://is.gd) service.

### Installation

#### Dependencies
+ [curl](https://curl.se/)
+ [jq](https://jqlang.github.io/jq/)
+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html) (optional - for (un)install)

To install, simply run:
```bash
sudo make install

# to uninstall
sudo make uninstall
```
 
### Usage
```bash
isgd -h (shows help message)
isgd "https://myurl.com/some/site/i/want/to/shorten"
```

### Notes
+ Make sure to respect is.gd's [API usage limits](https://is.gd/usagelimits.php)
+ is.gd's service appears to blacklist some vpn services

### License / Disclaimer
This project is licensed under the ISC license. (See LICENSE.md)

I take no responsibility for you blowing stuff up.

