PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
install:
	cp ./isgd.sh $(BINDIR)/isgd
uninstall:
	rm $(BINDIR)/isgd

