#!/bin/sh
# Copyright (C) 2023 klosure
# License: ISC

CURL_OPTS="-s"
RESP_FORMAT="format=json"
BASE_URL="https://is.gd/create.php"
CURL_URL="$BASE_URL?$RESP_FORMAT"

if ! [ -x "$(command -v curl)" ]; then
  echo 'Error: curl is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v jq)" ]; then
  echo 'Error: jq is not installed.' >&2
  exit 1
fi


fun_shorten () {
	L_ENCODED_URL="$(echo $1 | jq -sRr @uri)"
	#echo $L_ENCODED_URL
	L_REQ_URL="$CURL_URL&url=$L_ENCODED_URL"
	#echo $L_REQ_URL 
	L_JSON_RESP=$(curl $CURL_OPTS "$L_REQ_URL")
	#echo $L_JSON_RESP
	L_SHORT_URL=$(echo $L_JSON_RESP | jq '.shorturl')
	#echo $L_SHORT_URL
	L_CUT_URL=$(echo $L_SHORT_URL | cut -c2- | rev | cut -c2- | rev)
	echo $L_CUT_URL
	unset L_ENCODED_URL L_REQ_URL L_JSON_RESP L_SHORT_URL L_CUT_URL
}

fun_display_help () {
	echo -e "\nGive as argument an url address (in quotes) to shorten.\n\nUsage: $0 [url]\n"
}

case $1 in
	"-h"|"--help") fun_display_help ;;
	*)	fun_shorten $1 ;;
esac

exit 0
